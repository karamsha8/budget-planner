import 'package:budget_planner_app/utils/size_config.dart';
import 'package:flutter/material.dart';

class BudgetPlannerCard extends StatelessWidget {
  final double widthCard;
  final double heightCard;
  final double borderRadiusCard;
  final double widthImageCard;
  final double heightImageCard;
  final String pathImage;

  BudgetPlannerCard({
    required this.widthCard,
    required this.heightCard,
    this.borderRadiusCard = 0,
    this.widthImageCard = 0,
    this.heightImageCard = 0,
    required this.pathImage,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: SizeConfig.scaleHeight(heightCard),
      width: SizeConfig.scaleWidth(widthCard),
      child: Card(
        color: Colors.white,
        elevation: 7,
        shape: RoundedRectangleBorder(
          borderRadius:
              BorderRadius.circular(SizeConfig.scaleWidth(borderRadiusCard)),
        ),
        child: Image.asset(
          pathImage,
          width: SizeConfig.scaleWidth(widthImageCard),
          height: SizeConfig.scaleHeight(heightImageCard),
        ),
      ),
    );
  }
}
