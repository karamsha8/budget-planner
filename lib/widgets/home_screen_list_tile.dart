import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:flutter/material.dart';

class HomeScreenListTile extends StatelessWidget {
  final Widget? trailing;
  final Widget? subtitle;
  final String title;
  final String pathIcon;
  final FontWeight fontWeight;
  final GestureTapCallback? onTap;

  HomeScreenListTile({
    this.trailing,
    this.subtitle,
    this.title = 'Food',
    this.pathIcon = 'images/pic-food.png',
    this.fontWeight = FontWeight.w600,
    this.onTap
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTap,
      contentPadding: EdgeInsets.zero,
      title: BudgetPlannerText(
        text: title,
        fontsize: 15,
        fontWeight: fontWeight,
        color: Color(0xFF0D0E56),
      ),
      subtitle: subtitle,
      leading: Image.asset(pathIcon),
      trailing: trailing,

    );
  }
}
