import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:flutter/material.dart';

class CreateAccountCardRow extends StatelessWidget {
  final String title;
  final TextInputType textInputType;
  final String hintTextField;

  CreateAccountCardRow({
    required this.title,
    this.textInputType = TextInputType.text,
    required this.hintTextField,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        BudgetPlannerText(
          text: title,
          fontWeight: FontWeight.w500,
          fontsize: SizeConfig.scaleTextFont(15),
          color: Color(0xFF181819),
        ),
        Spacer(),
        Expanded(
          child: TextField(
            cursorWidth: 1,
            textAlign: TextAlign.end,
            keyboardType: textInputType,
            decoration: InputDecoration(
              contentPadding: EdgeInsetsDirectional.only(
                top: SizeConfig.scaleHeight(22),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.transparent,
                  width: SizeConfig.scaleWidth(1),
                ),
                borderRadius: BorderRadius.circular(
                  SizeConfig.scaleWidth(11),
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.transparent,
                  width: 1,
                ),
                borderRadius: BorderRadius.circular(
                  SizeConfig.scaleWidth(11),
                ),
              ),
              hintText: hintTextField,
              hintStyle: TextStyle(
                color: Color(0xFF7B7C98),
                fontSize: SizeConfig.scaleTextFont(15),
                fontFamily: 'Montserrat',
              ),
            ),
          ),
        ),
      ],
    );
  }
}
