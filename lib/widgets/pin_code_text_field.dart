import 'package:budget_planner_app/utils/size_config.dart';
import 'package:flutter/material.dart';

class PinCodeTextField extends StatelessWidget {


  final FocusNode? focusNode;
  final ValueChanged<String>? onChanged;
  Color fillColor;


  PinCodeTextField({
    this.focusNode,
    this.onChanged,
    this.fillColor = Colors.white,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.scaleHeight(45),
      width: SizeConfig.scaleWidth(45),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Color(0xFF000000).withAlpha(20),
            offset: Offset(0, 0),
            blurRadius: 6,
            spreadRadius: 1,
          ),
        ],
      ),
      child: TextField(
        cursorWidth: 0,
        minLines: null,
        maxLines: null,
        expands: true,
        maxLength: 1,
        focusNode: focusNode,
        onChanged: onChanged,
        textAlign: TextAlign.center,
        style: TextStyle(
            fontSize: 23,
            fontWeight: FontWeight.bold,
            color: Colors.white,
            fontFamily: 'Montserrat'),
        decoration: InputDecoration(
          counterText: '',
          filled: true,
          fillColor:  Color(0xFF472FC8),// focusNode!.hasFocus ? Color(0xFF472FC8) : Colors.white,
          contentPadding: EdgeInsets.zero,
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(
              SizeConfig.scaleWidth(11),
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(
              SizeConfig.scaleWidth(11),
            ),
          ),
        ),
      ),
    );
  }
}
