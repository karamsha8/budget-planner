import 'package:budget_planner_app/utils/size_config.dart';
import 'package:flutter/material.dart';


class BudgetPlannerTextField extends StatelessWidget {
  final TextEditingController? textEditingController;
  final String? textError;
  final bool obscureText;
  final TextInputType keyboardType;
  final String hintTextField;
  final double borderWidth;
  final double hintFontSize;
  final FontWeight hintFontWeight;
  final String hintFontFamily;
  final double textSize;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final Color fillColor;
  final double paddingTop;
  final double paddingEnd;
  final double paddingBottom;
  final Decoration? decoration;


  BudgetPlannerTextField({
    this.obscureText = false,
    this.keyboardType = TextInputType.text,
    required this.hintTextField,
    this.borderWidth = 1,
    this.hintFontSize = 15,
    this.hintFontWeight = FontWeight.normal,
    this.hintFontFamily = 'Montserrat',
    this.textEditingController,
    this.textError,
    this.textSize = 15,
    this.suffixIcon,
    this.fillColor = Colors.white,
    this.paddingTop = 11,
    this.paddingBottom = 11,
    this.paddingEnd = 0,
    this.prefixIcon,
    this.decoration,
  });


  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: decoration,

      child: TextField(
        cursorColor: Colors.black,
        cursorWidth: 1,
        controller: textEditingController,
        style: TextStyle(
          fontSize: textSize,
        ),
        obscureText: obscureText,
        keyboardType: keyboardType,
        decoration: InputDecoration(
          fillColor: fillColor,
          filled: true,
          contentPadding: EdgeInsetsDirectional.only(
            start: SizeConfig.scaleWidth(20),
            top: SizeConfig.scaleHeight(paddingTop),
            bottom: SizeConfig.scaleHeight(paddingBottom),
            end: SizeConfig.scaleWidth(paddingEnd),
          ),
          errorText: textError,
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              width: SizeConfig.scaleWidth(1),
              color: Colors.red,
            ),
            borderRadius: BorderRadius.circular(
              SizeConfig.scaleWidth(11),
            ),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              width: SizeConfig.scaleWidth(1),
              color: Colors.red,
            ),
            borderRadius: BorderRadius.circular(
              SizeConfig.scaleWidth(11),
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.transparent,
              width: SizeConfig.scaleWidth(1),
            ),
            borderRadius: BorderRadius.circular(
              SizeConfig.scaleWidth(11),
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Color(0xFF9A9A9A),
              width: borderWidth,
            ),
            borderRadius: BorderRadius.circular(
              SizeConfig.scaleWidth(11),
            ),
          ),
          hintText: hintTextField,
          hintStyle: TextStyle(
            fontSize: hintFontSize,
            fontWeight: hintFontWeight,
            fontFamily: hintFontFamily,
            color: Color(0xFF7B7C98),
          ),
          suffixIcon: suffixIcon,
          prefixIcon: prefixIcon,
        ),
      ),
    );
  }
}
