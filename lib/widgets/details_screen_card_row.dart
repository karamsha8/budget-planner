import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:flutter/material.dart';

class DetailsScreenCardRow extends StatelessWidget {
  final String text;
  final String details;
  final VoidCallback? onpressed;


  DetailsScreenCardRow({
    required this.text,
    required this.details,
    this.onpressed,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        BudgetPlannerText(
          text: text,
          fontsize: 15,
          fontWeight: FontWeight.w500,
        ),
        Spacer(),
        Container(
          child: BudgetPlannerText(
            text: details,
            fontsize: 15,
            color: Color(0xFF7B7C98),
          ),
        ),
        SizedBox(width: SizeConfig.scaleWidth(2)),
        IconButton(
          padding: EdgeInsets.all(0),
          iconSize: SizeConfig.scaleWidth(15),
          alignment: Alignment.center,
          constraints: BoxConstraints(),
          onPressed: onpressed,
          icon: Icon(
            Icons.arrow_forward_ios,
            color: Color(0xFF555568),
            size: SizeConfig.scaleWidth(17),
          ),
        ),
      ],
    );
  }
}