import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:budget_planner_app/widgets/budget_planner_textField.dart';
import 'package:budget_planner_app/widgets/home_screen_list_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AllActionScreen extends StatelessWidget {
  const AllActionScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: BudgetPlannerText(
          text: AppLocalizations.of(context)!.all_action_screen_title,
          color: Color(0xFF0D0E56),
          fontWeight: FontWeight.bold,
          fontsize: SizeConfig.scaleTextFont(20),
        ),
        iconTheme: IconThemeData(color: Color(0xFF472FC8)),
      ),
      body: Padding(
        padding: EdgeInsetsDirectional.only(
          top: SizeConfig.scaleHeight(34),
          start: SizeConfig.scaleWidth(20),
          end: SizeConfig.scaleWidth(20),
        ),
        child: Column(
          children: [
            BudgetPlannerTextField(
              hintTextField: 'Search',
              fillColor: Color(0xFFF1F4FF),
              prefixIcon: Icon(
                Icons.search,
                color: Color(0xFF0D0E56),
                size: SizeConfig.scaleWidth(40),
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(43)),
            Expanded(
              child: ListView.builder(
                itemCount: 15,
                itemBuilder: (context, index) {
                  return HomeScreenListTile(
                    subtitle: BudgetPlannerText(
                      text: 'Pizza Day',
                      fontsize: 15,
                      color: Color(0xFF7B7C98),
                    ),
                    trailing: BudgetPlannerText(
                      text: '- \$223',
                      fontsize: 15,
                      fontWeight: FontWeight.w600,
                      color: Color(0xFFF33720),
                    ),
                  );
                },
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(30)),
          ],
        ),
      ),
    );
  }
}
