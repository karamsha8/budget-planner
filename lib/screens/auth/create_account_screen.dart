import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_button.dart';
import 'package:budget_planner_app/widgets/budget_planner_card.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:budget_planner_app/widgets/create_account_card_row.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CreateAccountScreen extends StatefulWidget {
  const CreateAccountScreen({Key? key}) : super(key: key);

  @override
  _CreateAccountScreenState createState() => _CreateAccountScreenState();
}

class _CreateAccountScreenState extends State<CreateAccountScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: Color(0xFF472FC8)),
      ),
      body: Padding(
        padding: EdgeInsetsDirectional.only(
          start: SizeConfig.scaleWidth(20),
          end: SizeConfig.scaleWidth(20),
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              BudgetPlannerCard(
                widthCard: 120,
                heightCard: 120,
                pathImage: 'images/login_icon.png',
                borderRadiusCard: 30,
                widthImageCard: 56.81,
                heightImageCard: 52.93,
              ),
              SizedBox(height: SizeConfig.scaleHeight(13)),
              BudgetPlannerText(
                text: AppLocalizations.of(context)!.create_account_title,
                color: Color(0xFF0D0E56),
                fontsize: 20,
                textAlign: TextAlign.center,
                fontWeight: FontWeight.bold,
              ),
              SizedBox(height: SizeConfig.scaleHeight(11)),
              BudgetPlannerText(
                text: AppLocalizations.of(context)!.create_account_supTitle,
                color: Color(0xFF7B7C98),
                fontsize: 15,
                textAlign: TextAlign.center,
              ),
              SizedBox(height: SizeConfig.scaleHeight(21)),
              Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadiusDirectional.circular(
                        SizeConfig.scaleWidth(10))),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.scaleWidth(15),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CreateAccountCardRow(
                        title: AppLocalizations.of(context)!
                            .create_account_text_field_name,
                        hintTextField: AppLocalizations.of(context)!
                            .create_account_text_field_name_hint,
                      ),
                      Divider(
                        thickness: 1,
                      ),
                      CreateAccountCardRow(
                        textInputType: TextInputType.emailAddress,
                        title: AppLocalizations.of(context)!
                            .create_account_text_field_email_address,
                        hintTextField: AppLocalizations.of(context)!
                            .create_account_text_field_name_hint,
                      ),
                      Divider(
                        thickness: 1,
                      ),
                      SizedBox(height: SizeConfig.scaleHeight(20)),
                      Row(
                        children: [
                          BudgetPlannerText(
                            text: AppLocalizations.of(context)!
                                .create_account_text_field_currency,
                            fontWeight: FontWeight.w500,
                            fontsize: SizeConfig.scaleTextFont(15),
                            color: Color(0xFF181819),
                          ),
                          Spacer(),
                          BudgetPlannerText(
                            text: AppLocalizations.of(context)!
                                .create_account_text_field_currency_type,
                            fontsize: SizeConfig.scaleTextFont(15),
                            color: Color(0xFF7B7C98),
                          ),
                          SizedBox(width: SizeConfig.scaleWidth(2)),
                          IconButton(
                            padding: EdgeInsets.all(0),
                            iconSize: SizeConfig.scaleWidth(15),
                            alignment: Alignment.center,
                            constraints: BoxConstraints(),
                            onPressed: () {},
                            icon: Icon(
                              Icons.arrow_forward_ios,
                              color: Color(0xFF555568),
                              size: SizeConfig.scaleWidth(17),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: SizeConfig.scaleHeight(20)),
                      Divider(
                        thickness: 1,
                      ),
                      CreateAccountCardRow(
                        textInputType: TextInputType.emailAddress,
                        title: AppLocalizations.of(context)!
                            .create_account_text_field_daily_limit,
                        hintTextField: AppLocalizations.of(context)!
                            .create_account_text_field_daily_limit_hint,
                      ),
                      Divider(
                        thickness: 1,
                      ),
                      SizedBox(height: SizeConfig.scaleHeight(20)),
                      GestureDetector(
                        onTap: (){
                          Navigator.pushNamed(context, '/pin_code_screen');
                        },

                        child: Container(
                          width: double.infinity,
                          child: BudgetPlannerText(
                            text: AppLocalizations.of(context)!
                                .create_account_text_field_your_pin,
                            fontWeight: FontWeight.w500,
                            fontsize: SizeConfig.scaleTextFont(15),
                            color: Color(0xFF181819),
                          ),
                        ),
                      ),
                      SizedBox(height: SizeConfig.scaleHeight(20)),
                    ],
                  ),
                ),
              ),
              SizedBox(height: SizeConfig.scaleHeight(102)),
              BudgetPlannerButton(
                onPressedPage: (){
                  Navigator.pushNamed(context, '/create_account_success');
                },
                textButton: AppLocalizations.of(context)!.create_account_btn_text,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
