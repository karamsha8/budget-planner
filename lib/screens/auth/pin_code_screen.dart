import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_button.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:budget_planner_app/widgets/pin_code_card_number.dart';
import 'package:budget_planner_app/widgets/pin_code_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class PinCodeScreen extends StatefulWidget {
  const PinCodeScreen({Key? key}) : super(key: key);

  @override
  _PinCodeScreenState createState() => _PinCodeScreenState();
}

class _PinCodeScreenState extends State<PinCodeScreen> {
  // Color selectedColor = Colors.white;
  // Color unSelectedColor = Colors.white;
  late String text;
  // late Color fillColors;

  late FocusNode _firstCodeFocusNode;
  late FocusNode _secondCodeFocusNode;
  late FocusNode _thirdCodeFocusNode;
  late FocusNode _forthCodeFocusNode;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _firstCodeFocusNode = FocusNode();
    _secondCodeFocusNode = FocusNode();
    _thirdCodeFocusNode = FocusNode();
    _forthCodeFocusNode = FocusNode();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _firstCodeFocusNode.dispose();
    _secondCodeFocusNode.dispose();
    _thirdCodeFocusNode.dispose();
    _forthCodeFocusNode.dispose();
    super.dispose();
  }

  // void myColor(){
  //   if(text.length == 1){
  //     fillColors = selectedColor;
  //   }else{
  //     fillColors = unSelectedColor;
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: Color(0xFF472FC8)),
      ),
      body: Padding(
        padding: EdgeInsetsDirectional.only(
            start: SizeConfig.scaleWidth(20),
            end: SizeConfig.scaleWidth(20),
            top: SizeConfig.scaleHeight(33)),
        child: Column(
          children: [
            BudgetPlannerText(
              text: AppLocalizations.of(context)!.pin_code_title,
              color: Color(0xFF0D0E56),
              fontsize: 20,
              textAlign: TextAlign.center,
              fontWeight: FontWeight.bold,
            ),
            SizedBox(height: SizeConfig.scaleHeight(6)),
            BudgetPlannerText(
              text: AppLocalizations.of(context)!.pin_code_supTitle,
              color: Color(0xFF7B7C98),
              fontsize: 15,
              textAlign: TextAlign.center,
            ),
            SizedBox(height: SizeConfig.scaleHeight(20)),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                PinCodeTextField(
                  onChanged: (text) {
                    if (text.length == 1) {
                      _secondCodeFocusNode.requestFocus();
                    }

                  },
                  focusNode: _firstCodeFocusNode,
                  // fillColor: selectedColor,
                ),
                SizedBox(width: SizeConfig.scaleWidth(12)),
                PinCodeTextField(
                  onChanged: (text) {
                    if (text.length == 1) {
                      _thirdCodeFocusNode.requestFocus();
                    }
                  },
                  focusNode: _secondCodeFocusNode,
                  // fillColor: selectedColor,
                ),
                SizedBox(width: SizeConfig.scaleWidth(12)),
                PinCodeTextField(
                  onChanged: (text) {
                    if (text.length == 1) {
                      _forthCodeFocusNode.requestFocus();
                    }
                  },
                  focusNode: _thirdCodeFocusNode,
                  // fillColor: selectedColor,
                ),
                SizedBox(width: SizeConfig.scaleWidth(12)),
                PinCodeTextField(
                  onChanged: (String text) {
                    if (text.length == 1) {
                      _forthCodeFocusNode.unfocus();
                    }
                  },
                  focusNode: _forthCodeFocusNode,
                  // fillColor: selectedColor,
                ),
              ],
            ),
            SizedBox(height: SizeConfig.scaleHeight(70)),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                PinCodeCardNumber(num: '1'),
                SizedBox(width: SizeConfig.scaleWidth(32)),
                PinCodeCardNumber(num: '2'),
                SizedBox(width: SizeConfig.scaleWidth(32)),
                PinCodeCardNumber(num: '3'),
              ],
            ),
            SizedBox(height: SizeConfig.scaleHeight(32)),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                PinCodeCardNumber(num: '4'),
                SizedBox(width: SizeConfig.scaleWidth(32)),
                PinCodeCardNumber(num: '5'),
                SizedBox(width: SizeConfig.scaleWidth(32)),
                PinCodeCardNumber(num: '6'),
              ],
            ),
            SizedBox(height: SizeConfig.scaleHeight(32)),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                PinCodeCardNumber(num: '7'),
                SizedBox(width: SizeConfig.scaleWidth(32)),
                PinCodeCardNumber(num: '8'),
                SizedBox(width: SizeConfig.scaleWidth(32)),
                PinCodeCardNumber(num: '9'),
              ],
            ),
            SizedBox(height: SizeConfig.scaleHeight(170)),
            BudgetPlannerButton(
              onPressedPage: (){},
              textButton: AppLocalizations.of(context)!.pin_code_text_btn,
            ),
          ],
        ),
      ),
    );
  }
}
