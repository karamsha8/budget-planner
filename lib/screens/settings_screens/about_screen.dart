import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_card.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AboutAppScreen extends StatelessWidget {
  const AboutAppScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: BudgetPlannerText(
          text: AppLocalizations.of(context)!.about_screen_title,
          color: Color(0xFF0D0E56),
          fontsize: SizeConfig.scaleTextFont(20),
          fontWeight: FontWeight.bold,
        ),
        centerTitle: true,
        iconTheme: IconThemeData(
          color: Color(0xFF472FC8),
        ),
      ),
      body: Stack(
        children: [
          Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                BudgetPlannerCard(
                  widthCard: 170,
                  heightCard: 170,
                  pathImage: 'images/logo.png',
                  borderRadiusCard: 45,
                ),
                SizedBox(height: SizeConfig.scaleHeight(30)),
                BudgetPlannerText(
                  text: AppLocalizations.of(context)!.about_screen_app_name,
                  color: Color(0xFF0D0E56),
                  fontWeight: FontWeight.w800,
                  fontsize: 24,
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: SizeConfig.scaleHeight(15)),
                BudgetPlannerText(
                  text: AppLocalizations.of(context)!.about_screen_project_name,
                  color: Color(0xFF0D0E56),
                  fontWeight: FontWeight.w300,
                  fontsize: 18,
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: SizeConfig.scaleHeight(5)),
                BudgetPlannerText(
                  text: AppLocalizations.of(context)!.about_screen_my_name,
                  color: Color(0xFF0D0E56).withOpacity(0.47),
                  fontWeight: FontWeight.w300,
                  fontsize: 15,
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: SizeConfig.scaleHeight(80)),
              ],
            ),
          ),
          Positioned(
            bottom: SizeConfig.scaleHeight(20),
            left: 0,
            right: 0,
            child: BudgetPlannerText(
              text: AppLocalizations.of(context)!.settings_screen_app_version,
              color: Color(0xFF0D0E56),
              fontWeight: FontWeight.w300,
              fontsize: 15,
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }
}
