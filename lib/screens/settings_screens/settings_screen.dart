import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:budget_planner_app/widgets/home_screen_list_tile.dart';
import 'package:budget_planner_app/widgets/profile_screen_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: BudgetPlannerText(
          text: AppLocalizations.of(context)!.settings_screen_title,
          color: Color(0xFF0D0E56),
          fontsize: SizeConfig.scaleTextFont(20),
          fontWeight: FontWeight.bold,
        ),
        centerTitle: true,
        iconTheme: IconThemeData(
          color: Color(0xFF472FC8),
        ),
      ),
      body: Padding(
        padding: EdgeInsetsDirectional.only(
          start: SizeConfig.scaleWidth(20),
          end: SizeConfig.scaleWidth(20),
          top: SizeConfig.scaleHeight(34),
        ),
        child: Stack(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BudgetPlannerText(
                  text: AppLocalizations.of(context)!.settings_screen_general,
                  color: Color(0xFF0D0E56),
                  fontsize: 15,
                  fontWeight: FontWeight.w500,
                ),
                SizedBox(height: SizeConfig.scaleHeight(15)),
                ProfileScreenCard(
                  onTap: (){
                    Navigator.pushNamed(context, '/about_app_screen');
                  },
                  titleColor: Color(0xFF0D0E56),
                  title: AppLocalizations.of(context)!.settings_screen_about_app,
                  leading: Icon(
                    Icons.info,
                    color: Color(0xFF0D0E56),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.black,
                    size: SizeConfig.scaleWidth(20),
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(10)),
                ProfileScreenCard(
                  titleColor: Color(0xFF0D0E56),
                  title: AppLocalizations.of(context)!.settings_screen_language,
                  leading: Icon(
                    Icons.language,
                    color: Color(0xFF0D0E56),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.black,
                    size: SizeConfig.scaleWidth(20),
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(10)),
                ProfileScreenCard(
                  onTap: (){
                    Navigator.pushNamedAndRemoveUntil(context, '/login_screen', (route) => false);
                  },
                  titleColor: Color(0xFF0D0E56),
                  title: AppLocalizations.of(context)!.settings_screen_logout,
                  leading: Icon(
                    Icons.logout,
                    color: Color(0xFF0D0E56),
                  ),
                ),
                SizedBox(height: SizeConfig.scaleHeight(25)),
                BudgetPlannerText(
                  text: AppLocalizations.of(context)!.settings_screen_account_data,
                  color: Color(0xFF0D0E56),
                  fontsize: 15,
                  fontWeight: FontWeight.w500,
                ),
                SizedBox(height: SizeConfig.scaleHeight(15)),
                ProfileScreenCard(
                  titleColor: Color(0xFFD50000),
                  title: AppLocalizations.of(context)!.settings_screen_clear_data,
                  leading: Icon(
                    Icons.delete_forever,
                    color: Color(0xFFD50000),
                  ),
                  onTap: (){},
                  borderWidth: 1,
                  borderColor: Color(0xFFD50000),
                ),
                SizedBox(height: SizeConfig.scaleHeight(10)),
                ProfileScreenCard(
                  titleColor: Colors.white,
                  title: AppLocalizations.of(context)!.settings_screen_clear_data,
                  leading: Icon(
                    Icons.person_remove,
                    color: Colors.white,
                  ),
                  onTap: (){},
                  cardColor: Color(0xFFD50000),
                ),

              ],
            ),
            Positioned(
              bottom: SizeConfig.scaleHeight(44),
              left: 0,
              right: 0,
              child: BudgetPlannerText(
                text: AppLocalizations.of(context)!.settings_screen_app_name,
                color: Color(0xFF0D0E56),
                fontWeight: FontWeight.w800,
                fontsize: 15,
                textAlign: TextAlign.center,
              ),
            ),
            Positioned(
              bottom: SizeConfig.scaleHeight(20),
              left: 0,
              right: 0,
              child: BudgetPlannerText(
                text: AppLocalizations.of(context)!.settings_screen_app_version,
                color: Color(0xFF0D0E56),
                fontWeight: FontWeight.w300,
                fontsize: 15,
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
