import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_button.dart';
import 'package:budget_planner_app/widgets/budget_planner_card.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:budget_planner_app/widgets/details_screen_card_row.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AddNewOperation extends StatefulWidget {
  const AddNewOperation({Key? key}) : super(key: key);

  @override
  _AddNewOperationState createState() => _AddNewOperationState();
}

class _AddNewOperationState extends State<AddNewOperation> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
            padding: EdgeInsetsDirectional.only(end: SizeConfig.scaleWidth(20)),
            constraints: BoxConstraints(),
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.close,
              color: Color(0xFF7B7C98),
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsetsDirectional.only(
            start: SizeConfig.scaleWidth(20),
            end: SizeConfig.scaleWidth(20),
            top: SizeConfig.scaleHeight(15),
          ),
          child: Column(
            children: [
              BudgetPlannerCard(
                widthCard: 120,
                heightCard: 120,
                pathImage: 'images/icon_wallet.png',
                borderRadiusCard: 30,
              ),
              SizedBox(height: SizeConfig.scaleHeight(13)),
              BudgetPlannerText(
                text: AppLocalizations.of(context)!.add_new_operation_title,
                fontsize: 20,
                fontWeight: FontWeight.bold,
                color: Color(0xFF0D0E56),
              ),
              SizedBox(height: SizeConfig.scaleHeight(10)),
              Container(
                alignment: Alignment.center,
                height: SizeConfig.scaleHeight(67),
                width: double.infinity,
                color: Colors.white,
                child: TextField(
                    cursorColor: Colors.black,
                    cursorWidth: 1,
                    cursorHeight: 20,
                    maxLines: 1,
                    style: TextStyle(
                      fontSize: SizeConfig.scaleTextFont(21),
                      fontFamily: 'Montserrat',
                      color: Colors.black,
                      fontWeight: FontWeight.w600,
                    ),
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      fillColor: Colors.white,
                      filled: true,
                      contentPadding: EdgeInsetsDirectional.only(
                        start: SizeConfig.scaleWidth(15),
                        top: SizeConfig.scaleHeight(15),
                        bottom: SizeConfig.scaleHeight(15),
                        end: SizeConfig.scaleWidth(15),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.transparent,
                          width: SizeConfig.scaleWidth(1),
                        ),
                        borderRadius: BorderRadius.circular(
                          SizeConfig.scaleWidth(11),
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.circular(
                          SizeConfig.scaleWidth(11),
                        ),
                      ),
                      hintText: '\$ 0,00',
                      hintStyle: TextStyle(
                        fontSize: 21,
                        fontFamily: 'Montserrat',
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                      ),
                    )),
                // BudgetPlannerText(
                //   text: '\$ 423',
                //   fontsize: 21,
                //   fontWeight: FontWeight.w600,
                //   color: Color(0xFF181819),
                // ),
              ),
              SizedBox(height: SizeConfig.scaleHeight(11)),
              Row(
                children: [
                  Expanded(
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Column(
                        children: [
                          SizedBox(height: SizeConfig.scaleHeight(23)),
                          Image.asset('images/expenses.png'),
                          SizedBox(height: SizeConfig.scaleHeight(8)),
                          BudgetPlannerText(
                            text: AppLocalizations.of(context)!
                                .add_new_category_expenses,
                            color: Color(0xFF181819),
                            fontWeight: FontWeight.w500,
                            fontsize: 15,
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(height: SizeConfig.scaleHeight(15)),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(width: SizeConfig.scaleWidth(10)),
                  Expanded(
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Column(
                        children: [
                          SizedBox(height: SizeConfig.scaleHeight(23)),
                          Image.asset('images/income.png'),
                          SizedBox(height: SizeConfig.scaleHeight(8)),
                          BudgetPlannerText(
                            text: AppLocalizations.of(context)!
                                .add_new_category_income,
                            color: Color(0xFF181819),
                            fontWeight: FontWeight.w500,
                            fontsize: 15,
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(height: SizeConfig.scaleHeight(15)),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: SizeConfig.scaleHeight(11)),
              Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadiusDirectional.circular(
                        SizeConfig.scaleWidth(10))),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: SizeConfig.scaleWidth(20),
                      vertical: SizeConfig.scaleHeight(22)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      DetailsScreenCardRow(
                        onpressed: (){
                          Navigator.pushNamed(context, '/categories_screen_2');
                        },
                        text: AppLocalizations.of(context)!
                            .details_screen_category,
                        details: 'Travel',
                      ),
                      Divider(
                        thickness: 1,
                        height: SizeConfig.scaleHeight(44),
                      ),
                      DetailsScreenCardRow(
                        text: AppLocalizations.of(context)!.details_screen_date,
                        details: 'Date',
                      ),
                      Divider(
                        thickness: 1,
                        height: SizeConfig.scaleHeight(44),
                      ),
                      DetailsScreenCardRow(
                        text: AppLocalizations.of(context)!
                            .details_screen_currency,
                        details: 'Currency',
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: SizeConfig.scaleHeight(112),
                child: Card(
                  elevation: 4,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  child: TextField(
                    cursorColor: Colors.black,
                    cursorWidth: 1,
                    cursorHeight: 20,
                    maxLines: 3,
                    style: TextStyle(
                      fontSize: SizeConfig.scaleTextFont(20),
                    ),
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      fillColor: Colors.white,
                      filled: true,
                      contentPadding: EdgeInsetsDirectional.only(
                        start: SizeConfig.scaleWidth(15),
                        top: SizeConfig.scaleHeight(15),
                        bottom: SizeConfig.scaleHeight(15),
                        end: SizeConfig.scaleWidth(15),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.transparent,
                          width: SizeConfig.scaleWidth(1),
                        ),
                        borderRadius: BorderRadius.circular(
                          SizeConfig.scaleWidth(11),
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.circular(
                          SizeConfig.scaleWidth(11),
                        ),
                      ),
                      hintText: 'Note',
                      hintStyle: TextStyle(
                        fontSize: 15,
                        fontFamily: 'Montserrat',
                        color: Color(0xFFBABAD7),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: SizeConfig.scaleHeight(30)),
              BudgetPlannerButton(
                onPressedPage: () {
                  Navigator.pushReplacementNamed(context, '/new_operation_success');
                },
                textButton:
                    AppLocalizations.of(context)!.add_new_operation_btn_text,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
