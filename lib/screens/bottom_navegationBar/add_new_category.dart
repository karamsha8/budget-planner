import 'package:budget_planner_app/models/contact.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_button.dart';
import 'package:budget_planner_app/widgets/budget_planner_card.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:budget_planner_app/widgets/budget_planner_textField.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AddNewCategory extends StatefulWidget {
  const AddNewCategory({Key? key}) : super(key: key);

  @override
  _AddNewCategoryState createState() => _AddNewCategoryState();
}

class _AddNewCategoryState extends State<AddNewCategory> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsetsDirectional.only(
          start: SizeConfig.scaleWidth(20),
          end: SizeConfig.scaleWidth(20),
          top: SizeConfig.scaleHeight(60),
        ),
        child: Column(
          children: [
            Row(
              // mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(
                  padding: EdgeInsets.zero,
                  constraints: BoxConstraints(),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(
                    Icons.close,
                    color: Color(0xFF7B7C98),
                  ),
                ),
              ],
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),
            BudgetPlannerCard(
              widthCard: 120,
              heightCard: 120,
              pathImage: 'images/icon_wallet.png',
              borderRadiusCard: 30,
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),
            BudgetPlannerText(
              text: AppLocalizations.of(context)!.add_new_category_title,
              fontsize: 20,
              fontWeight: FontWeight.bold,
              color: Color(0xFF0D0E56),
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(50),
            ),
            Row(
              children: [
                Expanded(
                  child: Card(
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: Column(
                      children: [
                        SizedBox(height: SizeConfig.scaleHeight(23)),
                        Image.asset('images/expenses.png'),
                        SizedBox(height: SizeConfig.scaleHeight(8)),
                        BudgetPlannerText(
                          text: AppLocalizations.of(context)!
                              .add_new_category_expenses,
                          color: Color(0xFF181819),
                          fontWeight: FontWeight.w500,
                          fontsize: 15,
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: SizeConfig.scaleHeight(15)),
                      ],
                    ),
                  ),
                ),
                SizedBox(width: SizeConfig.scaleWidth(10)),
                Expanded(
                  child: Card(
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: Column(
                      children: [
                        SizedBox(height: SizeConfig.scaleHeight(23)),
                        Image.asset('images/income.png'),
                        SizedBox(height: SizeConfig.scaleHeight(8)),
                        BudgetPlannerText(
                          text: AppLocalizations.of(context)!
                              .add_new_category_income,
                          color: Color(0xFF181819),
                          fontWeight: FontWeight.w500,
                          fontsize: 15,
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: SizeConfig.scaleHeight(15)),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: SizeConfig.scaleHeight(12)),
            BudgetPlannerTextField(
              paddingTop: 22,
              paddingBottom: 22,
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    // color:
                    color: Color(0xFFE9E7F1).withAlpha(250),
                    offset: Offset(0, 3),
                    blurRadius: 14,
                    spreadRadius: 0,
                  ),
                ],
              ),
              hintTextField: AppLocalizations.of(context)!
                  .add_new_category_text_field_hint,
            ),
            SizedBox(height: SizeConfig.scaleHeight(30)),
            BudgetPlannerButton(
              onPressedPage: (){},
              textButton:
                  AppLocalizations.of(context)!.add_new_category_btn_text,
            ),
          ],
        ),
      ),
    );
  }
}
