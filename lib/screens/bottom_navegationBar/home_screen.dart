import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_button.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:budget_planner_app/widgets/home_screen_list_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsetsDirectional.only(
          start: SizeConfig.scaleWidth(20),
          end: SizeConfig.scaleWidth(20),
          top: SizeConfig.scaleHeight(50)),
      child: Column(
        children: [
          SizedBox(
            height: SizeConfig.scaleHeight(260),
            width: SizeConfig.scaleWidth(260),
            child: Card(
              elevation: 7,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(130),
                // side: BorderSide(
                //   color: Colors.transparent,
                //   width: 1,
                // ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    // crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        '\$ ',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: SizeConfig.scaleTextFont(18.5),
                            fontWeight: FontWeight.w600,
                            fontFamily: 'Montserrat'),
                      ),
                      BudgetPlannerText(
                        text: '765',
                        fontsize: SizeConfig.scaleTextFont(40),
                        fontWeight: FontWeight.w600,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                  BudgetPlannerText(
                    text: AppLocalizations.of(context)!.home_screen_spent_today,
                    color: Color(0xFF7B7C98),
                    fontsize: SizeConfig.scaleTextFont(16.5),
                  ),
                  Divider(
                    thickness: 1,
                    indent: SizeConfig.scaleWidth(33.55),
                    endIndent: SizeConfig.scaleWidth(33.55),
                    height: 30,
                  ),
                  BudgetPlannerText(
                    text: AppLocalizations.of(context)!
                        .home_screen_balance_for_today,
                    color: Color(0xFF7B7C98),
                    fontsize: SizeConfig.scaleTextFont(16.5),
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(6)),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    // crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        '\$ ',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: SizeConfig.scaleTextFont(13),
                          fontWeight: FontWeight.w600,
                          fontFamily: 'Montserrat',
                          color: Color(0xFF00BEA1),
                        ),
                      ),
                      BudgetPlannerText(
                        text: '1267',
                        fontsize: SizeConfig.scaleTextFont(23),
                        fontWeight: FontWeight.w600,
                        textAlign: TextAlign.start,
                        color: Color(0xFF00BEA1),
                      ),
                    ],
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(10)),
                ],
              ),
            ),
          ),
          SizedBox(height: SizeConfig.scaleHeight(23)),
          Row(
            children: [
              BudgetPlannerText(
                text: AppLocalizations.of(context)!.home_screen_last_actions,
                fontsize: SizeConfig.scaleTextFont(20),
                fontWeight: FontWeight.bold,
                textAlign: TextAlign.center,
                color: Color(0xFF0D0E56),
              ),
            ],
          ),
          Card(
            elevation: 4,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Padding(
              padding: EdgeInsetsDirectional.only(
                start: SizeConfig.scaleWidth(12),
                end: SizeConfig.scaleWidth(12),
                top: SizeConfig.scaleHeight(20),
                bottom: SizeConfig.scaleHeight(25),
              ),
              child: Column(
                children: [
                  BudgetPlannerText(
                    text: 'Today',
                    fontsize: SizeConfig.scaleTextFont(12),
                    fontWeight: FontWeight.w600,
                    color: Color(0xFFB9BACE),
                  ),
                  HomeScreenListTile(
                    onTap: (){
                      Navigator.pushNamed(context, '/details_screen');
                    },
                    subtitle: BudgetPlannerText(
                      text: 'Pizza Day',
                      fontsize: 15,
                      color: Color(0xFF7B7C98),
                    ),
                    trailing: BudgetPlannerText(
                        text: '- \$223',
                        fontsize: 15,
                        fontWeight: FontWeight.w600,
                        color: Color(0xFFF33720),
                      ),
                  ),
                  Divider(
                    thickness: 0.3,
                    indent: SizeConfig.scaleWidth(12),
                    endIndent: SizeConfig.scaleWidth(12),
                    height: 10,
                  ),
                  HomeScreenListTile(
                    subtitle: BudgetPlannerText(
                      text: 'Pizza Day',
                      fontsize: 15,
                      color: Color(0xFF7B7C98),
                    ),
                    trailing: BudgetPlannerText(
                      text: '- \$223',
                      fontsize: 15,
                      fontWeight: FontWeight.w600,
                      color: Color(0xFFF33720),
                    ),
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(25)),
                  Padding(
                    padding: EdgeInsetsDirectional.only(
                      start: SizeConfig.scaleWidth(4),
                      end: SizeConfig.scaleWidth(4),
                    ),
                    child: BudgetPlannerButton(
                      onPressedPage: () {
                        Navigator.pushNamed(context, '/all_actions_screen');
                      },
                      textButton:
                          AppLocalizations.of(context)!.home_screen_btn_text,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
