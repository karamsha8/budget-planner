import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_textField.dart';
import 'package:flutter/material.dart';

class TipsScreen extends StatefulWidget {
  const TipsScreen({Key? key}) : super(key: key);

  @override
  _TipsScreenState createState() => _TipsScreenState();
}

class _TipsScreenState extends State<TipsScreen> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SingleChildScrollView(
          child: GridView(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: 10,
              mainAxisSpacing: 17,
              childAspectRatio: 182 / 230,
            ),
            children: [
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                clipBehavior: Clip.antiAlias,
                child: Column(
                  children: [
                    SizedBox(height: SizeConfig.scaleHeight(13.5),),
                    Image(
                      image: AssetImage(
                        'images/on_boarding_layer_3.png',
                      ),
                      height: SizeConfig.scaleHeight(157),
                      width: SizeConfig.scaleWidth(182),
                    ),
                    Text(
                      ' How to save \n budget 10 tips',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Color(0xff0D0E56),
                          fontSize: 15,
                          fontWeight: FontWeight.w600),
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(6),),
                    Text(
                      ' 3 month ago',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Color(0xff0D0E56),
                          fontSize: 13,
                          fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
              ),
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                clipBehavior: Clip.antiAlias,
                child: Column(
                  children: [
                    SizedBox(height: SizeConfig.scaleHeight(13.5),),
                    Image(
                      image: AssetImage(
                        'images/on_boarding_layer_2.png',
                      ),
                      height: SizeConfig.scaleHeight(157),
                      width: SizeConfig.scaleWidth(182),
                    ),
                    Text(
                      ' See where your \n money is going',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Color(0xff0D0E56),
                          fontSize: 15,
                          fontWeight: FontWeight.w600),
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(6),),
                    Text(
                      ' 12 month ago',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Color(0xff0D0E56),
                          fontSize: 13,
                          fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
              ),
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                clipBehavior: Clip.antiAlias,
                child: Column(
                  children: [
                    SizedBox(height: SizeConfig.scaleHeight(13.5),),
                    Image(
                      image: AssetImage(
                        'images/on_boarding_layer_4.png',
                      ),
                      height: SizeConfig.scaleHeight(157),
                      width: SizeConfig.scaleWidth(182),
                    ),
                    Text(
                      ' Take hold of your \n finances',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Color(0xff0D0E56),
                          fontSize: 15,
                          fontWeight: FontWeight.w600),
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(6),),
                    Text(
                      ' 3 month ago',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Color(0xff0D0E56),
                          fontSize: 13,
                          fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
              ),
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                clipBehavior: Clip.antiAlias,
                child: Column(
                  children: [
                    SizedBox(height: SizeConfig.scaleHeight(13.5),),
                    Image(
                      image: AssetImage(
                        'images/on_boarding_layer_1.png',
                      ),
                      height: SizeConfig.scaleHeight(157),
                      width: SizeConfig.scaleWidth(182),
                    ),
                    Text(
                      ' Take hold of your  \n finances',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Color(0xff0D0E56),
                          fontSize: 15,
                          fontWeight: FontWeight.w600),
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(6),),
                    Text(
                      ' 12 month ago',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Color(0xff0D0E56),
                          fontSize: 13,
                          fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
              ),
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                clipBehavior: Clip.antiAlias,
                child: Column(
                  children: [
                    SizedBox(height: SizeConfig.scaleHeight(13.5),),
                    Image(
                      image: AssetImage(
                        'images/on_boarding_layer_3.png',
                      ),
                      height: SizeConfig.scaleHeight(157),
                      width: SizeConfig.scaleWidth(182),
                    ),
                    Text(
                      ' How to save \n budget 10 tips',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Color(0xff0D0E56),
                          fontSize: 15,
                          fontWeight: FontWeight.w600),
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(6),),
                    Text(
                      ' 3 month ago',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Color(0xff0D0E56),
                          fontSize: 13,
                          fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
              ),
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                clipBehavior: Clip.antiAlias,
                child: Column(
                  children: [
                    SizedBox(height: SizeConfig.scaleHeight(13.5),),
                    Image(
                      image: AssetImage(
                        'images/on_boarding_layer_2.png',
                      ),
                      height: SizeConfig.scaleHeight(157),
                      width: SizeConfig.scaleWidth(182),
                    ),
                    Text(
                      ' See where your \n money is going',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Color(0xff0D0E56),
                          fontSize: 15,
                          fontWeight: FontWeight.w600),
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(6),),
                    Text(
                      ' 12 month ago',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Color(0xff0D0E56),
                          fontSize: 13,
                          fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
