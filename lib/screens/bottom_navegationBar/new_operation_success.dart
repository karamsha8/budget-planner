import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_card.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class NewOperationSuccess extends StatefulWidget {
  const NewOperationSuccess({Key? key}) : super(key: key);

  @override
  _NewOperationSuccessState createState() => _NewOperationSuccessState();
}

class _NewOperationSuccessState extends State<NewOperationSuccess> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            BudgetPlannerCard(
              widthCard: 120,
              heightCard: 120,
              pathImage: 'images/like.png',
            ),
            SizedBox(height: SizeConfig.scaleHeight(29)),
            BudgetPlannerText(
              text: AppLocalizations.of(context)!.new_operation_success_title,
              color: Color(0xFF0D0E56),
              fontsize: 15,
              textAlign: TextAlign.center,
              fontWeight: FontWeight.bold,
            ),
            SizedBox(height: SizeConfig.scaleHeight(12)),
            BudgetPlannerText(
              text: AppLocalizations.of(context)!.new_operation_success_subtitle,
              color: Color(0xFF7B7C98),
              fontsize: 15,
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
