import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/budget_planner_text.dart';
import 'package:budget_planner_app/widgets/home_screen_list_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CategoriesScreen extends StatefulWidget {
  const CategoriesScreen({Key? key}) : super(key: key);

  @override
  _CategoriesScreenState createState() => _CategoriesScreenState();
}

class _CategoriesScreenState extends State<CategoriesScreen> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsetsDirectional.only(
        start: SizeConfig.scaleWidth(20),
        end: SizeConfig.scaleWidth(20),
        top: SizeConfig.scaleHeight(15),
      ),
      child: Column(
        children: [
          Container(
            clipBehavior: Clip.antiAlias,
            decoration: BoxDecoration(
                color: Color(0xFFF1F4FF),
                borderRadius: BorderRadius.circular(SizeConfig.scaleWidth(40))),
            child: TabBar(
              // indicatorWeight: double.infinity,
              indicator: BoxDecoration(
                  color: Color(0xFF472FC8),
                  borderRadius: BorderRadius.circular(40)),
              indicatorColor: Color(0xFF472FC8),
              unselectedLabelColor: Color(0xFF181819),
              labelColor: Colors.white,
              labelStyle: TextStyle(
                fontSize: 13,
                fontWeight: FontWeight.bold,
                fontFamily: 'Montserrat',
              ),
              tabs: [
                Tab(
                  text: AppLocalizations.of(context)!
                      .category_screen_tab_bar_expenses,
                ),
                Tab(
                  text: AppLocalizations.of(context)!
                      .category_screen_tab_bar_income,
                ),
              ],
            ),
          ),
          SizedBox(height: SizeConfig.scaleHeight(20)),
          Expanded(
            child: TabBarView(
              children: [
                Card(
                  elevation: 3,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Padding(
                    padding: EdgeInsetsDirectional.only(
                      start: SizeConfig.scaleWidth(12),
                      end: SizeConfig.scaleWidth(12),
                      top: SizeConfig.scaleHeight(20),
                      bottom: SizeConfig.scaleHeight(5),
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          HomeScreenListTile(
                            fontWeight: FontWeight.w500,
                            subtitle: BudgetPlannerText(
                              text: '20 Action',
                              fontsize: 11,
                              fontWeight: FontWeight.w500,
                              color: Color(0xFF7B7C98),
                            ),
                          ),
                          Divider(
                            thickness: 0.3,
                            indent: SizeConfig.scaleWidth(12),
                            endIndent: SizeConfig.scaleWidth(12),
                            height: 10,
                          ),
                          HomeScreenListTile(
                            fontWeight: FontWeight.w500,
                            subtitle: BudgetPlannerText(
                              text: '20 Action',
                              fontsize: 11,
                              fontWeight: FontWeight.w500,
                              color: Color(0xFF7B7C98),
                            ),
                          ),
                          Divider(
                            thickness: 0.3,
                            indent: SizeConfig.scaleWidth(12),
                            endIndent: SizeConfig.scaleWidth(12),
                            height: 10,
                          ),
                          HomeScreenListTile(
                            fontWeight: FontWeight.w500,
                            subtitle: BudgetPlannerText(
                              text: '20 Action',
                              fontsize: 11,
                              fontWeight: FontWeight.w500,
                              color: Color(0xFF7B7C98),
                            ),
                          ),
                          Divider(
                            thickness: 0.3,
                            indent: SizeConfig.scaleWidth(12),
                            endIndent: SizeConfig.scaleWidth(12),
                            height: 10,
                          ),
                          HomeScreenListTile(
                            fontWeight: FontWeight.w500,
                            subtitle: BudgetPlannerText(
                              text: '20 Action',
                              fontsize: 11,
                              fontWeight: FontWeight.w500,
                              color: Color(0xFF7B7C98),
                            ),
                          ),
                          Divider(
                            thickness: 0.3,
                            indent: SizeConfig.scaleWidth(12),
                            endIndent: SizeConfig.scaleWidth(12),
                            height: 10,
                          ),
                          HomeScreenListTile(
                            fontWeight: FontWeight.w500,
                            subtitle: BudgetPlannerText(
                              text: '20 Action',
                              fontsize: 11,
                              fontWeight: FontWeight.w500,
                              color: Color(0xFF7B7C98),
                            ),
                          ),
                          Divider(
                            thickness: 0.3,
                            indent: SizeConfig.scaleWidth(12),
                            endIndent: SizeConfig.scaleWidth(12),
                            height: 10,
                          ),
                          HomeScreenListTile(
                            fontWeight: FontWeight.w500,
                            subtitle: BudgetPlannerText(
                              text: '20 Action',
                              fontsize: 11,
                              fontWeight: FontWeight.w500,
                              color: Color(0xFF7B7C98),
                            ),
                          ),
                          Divider(
                            thickness: 0.3,
                            indent: SizeConfig.scaleWidth(12),
                            endIndent: SizeConfig.scaleWidth(12),
                            height: 10,
                          ),
                          HomeScreenListTile(
                            fontWeight: FontWeight.w500,
                            pathIcon: 'images/ic-other.png',
                            title: 'Others',
                            subtitle: BudgetPlannerText(
                              text: '20 Action',
                              fontsize: 11,
                              fontWeight: FontWeight.w500,
                              color: Color(0xFF7B7C98),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Card(
                  elevation: 4,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Padding(
                    padding: EdgeInsetsDirectional.only(
                      start: SizeConfig.scaleWidth(12),
                      end: SizeConfig.scaleWidth(12),
                      top: SizeConfig.scaleHeight(20),
                      bottom: SizeConfig.scaleHeight(5),
                    ),
                    child: Column(
                      children: [
                        HomeScreenListTile(
                          title: 'Premium',
                          pathIcon: 'images/premium.png',
                          fontWeight: FontWeight.w500,
                          subtitle: BudgetPlannerText(
                            text: '20 Action',
                            fontsize: 11,
                            fontWeight: FontWeight.w500,
                            color: Color(0xFF7B7C98),
                          ),
                        ),
                        Divider(
                          thickness: 0.3,
                          indent: SizeConfig.scaleWidth(12),
                          endIndent: SizeConfig.scaleWidth(12),
                          height: 10,
                        ),
                        HomeScreenListTile(
                          title: 'Premium',
                          pathIcon: 'images/premium.png',
                          fontWeight: FontWeight.w500,
                          subtitle: BudgetPlannerText(
                            text: '20 Action',
                            fontsize: 11,
                            fontWeight: FontWeight.w500,
                            color: Color(0xFF7B7C98),
                          ),
                        ),
                        Divider(
                          thickness: 0.3,
                          indent: SizeConfig.scaleWidth(12),
                          endIndent: SizeConfig.scaleWidth(12),
                          height: 10,
                        ),
                        HomeScreenListTile(
                          title: 'Premium',
                          pathIcon: 'images/premium.png',
                          fontWeight: FontWeight.w500,
                          subtitle: BudgetPlannerText(
                            text: '20 Action',
                            fontsize: 11,
                            fontWeight: FontWeight.w500,
                            color: Color(0xFF7B7C98),
                          ),
                        ),
                        Divider(
                          thickness: 0.3,
                          indent: SizeConfig.scaleWidth(12),
                          endIndent: SizeConfig.scaleWidth(12),
                          height: 10,
                        ),
                        HomeScreenListTile(
                          title: 'Premium',
                          pathIcon: 'images/premium.png',
                          fontWeight: FontWeight.w500,
                          subtitle: BudgetPlannerText(
                            text: '20 Action',
                            fontsize: 11,
                            fontWeight: FontWeight.w500,
                            color: Color(0xFF7B7C98),
                          ),
                        ),
                        Divider(
                          thickness: 0.3,
                          indent: SizeConfig.scaleWidth(12),
                          endIndent: SizeConfig.scaleWidth(12),
                          height: 10,
                        ),
                        HomeScreenListTile(
                          fontWeight: FontWeight.w500,
                          pathIcon: 'images/ic-other.png',
                          title: 'Others',
                          subtitle: BudgetPlannerText(
                            text: '20 Action',
                            fontsize: 11,
                            fontWeight: FontWeight.w500,
                            color: Color(0xFF7B7C98),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: SizeConfig.scaleHeight(10)),
        ],
      ),
    );
  }
}
