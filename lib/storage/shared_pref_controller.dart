import 'package:budget_planner_app/storage/pref_Keys.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefController{


  static final SharedPrefController _instance = SharedPrefController._internal();

  late SharedPreferences _sharedPreferences;

  factory SharedPrefController(){
    return _instance;
  }

  SharedPrefController._internal();

  Future<void> initSharedPreferences() async{
    _sharedPreferences = await SharedPreferences.getInstance();
  }

  SharedPreferences get preferences => _sharedPreferences;


  // LOCALIZATOINS
  Future<bool> changeLanguage(String langCode) async {
   return await _sharedPreferences.setString(PrefKeys.LANG_CODE_KEY, langCode);
  }

  String currentLanguage(){
    return _sharedPreferences.getString(PrefKeys.LANG_CODE_KEY) ?? 'en';
  }


}