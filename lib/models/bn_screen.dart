import 'package:flutter/material.dart';

class BottomNavigationScreen {
  IconData _iconData;
  String _title;
  Widget _widget;


  BottomNavigationScreen(
    this._title,
    this._widget,
    this._iconData,
  );

  Widget get widget => _widget;

  set widget(Widget value) {
    _widget = value;
  }

  String get title => _title;

  set title(String value) {
    _title = value;
  }

  IconData get iconData => _iconData;

  set iconData(IconData value) {
    _iconData = value;
  }
}
