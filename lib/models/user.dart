import 'package:budget_planner/models/db_table.dart';

class User extends DbTable {
  late int id;
  late String name;
  late String email;
  late int currencyId;
  late double dayLimit;
  late int pin;

  static const TABLE_NAME = 'users';

  User.fromMap(Map<String, dynamic> rowMap) : super.fromMap(rowMap) {
    id = rowMap['id'];
    name = rowMap['name'];
    email = rowMap['email'];
    currencyId = rowMap['currencyId'];
    dayLimit = rowMap['dayLimit'];
    pin = rowMap['pin'];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['id'] = id;
    map['name'] = name;
    map['email'] = email;
    map['currencyId'] = currencyId;
    map['dayLimit'] = dayLimit;
    map['pin'] = pin;
    return map;
  }
}
